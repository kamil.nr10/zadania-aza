const closeBtn = document.querySelector('.close');

const closeModal = () => {
    const modal = document.querySelector('.modal');
    modal.style.display = 'none';
}


closeBtn.addEventListener('click', closeModal)