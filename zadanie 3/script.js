const showBtn = document.querySelector('.show');

const showMenu = () => {
    const div = document.querySelector('.menu');

    div.classList.toggle('active');
}

showBtn.addEventListener('click', showMenu)